/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2018
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app_msg_handler.c
 * - Application message handlers
 * ----------------------------------------------------------------------------
 * $Revision: 1.4 $
 * $Date: 2018/07/05 19:02:20 $
 * ------------------------------------------------------------------------- */

#include <app.h>

/* Configuration pre-set in app_config.c */
extern struct gapm_set_dev_config_cmd   devConfigCmd;
extern struct gapm_start_connection_cmd startConnectionCmd;

/* ----------------------------------------------------------------------------
 * Function      : void APP_GAPM_Handler(ke_msg_id_t const msg_id,
 *                                     void const *param,
 *                                     ke_task_id_t const dest_id,
 *                                     ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handle GAPM messages that need application action
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void APP_GAPM_Handler(ke_msg_id_t const msg_id, void const *param,
                      ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    switch(msg_id)
    {
        case GAPM_CMP_EVT:
        {
            const struct gapm_cmp_evt *p = param;
			/* Reset completed. Apply device configuration. */
            if(p->operation == GAPM_RESET)
            {
                GAPM_SetDevConfigCmd(&devConfigCmd);
                /* Trigger a GAPM_CMP_EVT / GAPM_SET_DEV_CONFIG when finished.
                 * BASC (ble_basc.c) monitors this event before adding the profile/service to the stack. */
            }
            else if((p->operation == GAPM_RESOLV_ADDR) && /* IRK not found for address */
                    (p->status == GAP_ERR_NOT_FOUND))
            {
                PRINTF("\n\rGAPM_CMP_EVT / GAPM_RESOLV_ADDR. Status = NOT FOUND");
                struct gapc_connection_cfm cfm;
                uint8_t conidx = KE_IDX_GET(dest_id);
                APP_SetConnectionCfmParams(conidx, &cfm);
                GAPC_ConnectionCfm(conidx, &cfm); /* Confirm connection without LTK. */
            }
        }
        break;

        case GAPM_ADDR_SOLVED_IND: /* Private address resolution was successful */
        {
            PRINTF("\n\rGAPM_ADDR_SOLVED_IND");
            struct gapc_connection_cfm cfm;
            uint8_t conidx = KE_IDX_GET(dest_id);
            APP_SetConnectionCfmParams(conidx, &cfm);
            GAPC_ConnectionCfm(conidx, &cfm); /* Send connection confirmation with LTK */
        }
        break;

        case GAPM_PROFILE_ADDED_IND:
        {
            /* This event is generated when a standard profile is added to the stack.
             * In this application, BASC is the only standard profile used. */

            if(GAPM_GetProfileAddedCount() == APP_NUM_STD_PRF) /* If all profiles are added */
            {     
                GAPM_StartConnectionCmd(&startConnectionCmd); /* start connection */

                /* Start the LED periodic timer. LED blinks according to the
                 * number of connected peers. See APP_LED_Timeout_Handler. */
                ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));
            }
        }
        break;
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void APP_GAPC_Handler(ke_msg_id_t const msg_id,
 *                                     void const *param,
 *                                     ke_task_id_t const dest_id,
 *                                     ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Handle GAPC messages that need application action
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void APP_GAPC_Handler(ke_msg_id_t const msg_id, void const *param,
                      ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    uint8_t conidx = KE_IDX_GET(src_id);

    switch(msg_id)
    {
        case GAPC_CONNECTION_REQ_IND:
        {
            const struct gapc_connection_req_ind* p = param;
            PRINTF("\n\rGAPC_CONNECTION_REQ_IND: ");
            PRINTF("\n\r  ADDR: ");
            for (uint8_t i = 0;i < GAP_BD_ADDR_LEN; i++)
                PRINTF("%d ", p->peer_addr.addr[i]);
            PRINTF("\n\r  ADDR_TYPE: %d", p->peer_addr_type);

#if CFG_BOND_LIST_IN_NVR2
            if(GAP_IsAddrPrivateResolvable(p->peer_addr.addr, p->peer_addr_type) &&
               BondList_Size() > 0)
            {
                PRINTF("\n\r    Starting GAPM_ResolvAddrCmd\n\r");
                GAPM_ResolvAddrCmd(conidx, p->peer_addr.addr, 0, NULL);
            }
            else
#endif
            {
                struct gapc_connection_cfm cfm;
                APP_SetConnectionCfmParams(conidx, &cfm);
                GAPC_ConnectionCfm(conidx, &cfm); /* Send connection confirmation */

#if CFG_BOND_LIST_IN_NVR2
                if(GAPC_IsBonded(conidx)) /* If previously bonded, send encryption request */
                {
                    GAPC_EncryptCmd(conidx, GAPC_GetBondInfo(conidx)->EDIV,
                                    GAPC_GetBondInfo(conidx)->RAND,
                                    GAPC_GetBondInfo(conidx)->LTK, GAP_KEY_LEN);
                    PRINTF("\n\r    Sending GAPC_EncryptCmd");
                }
                else /* If not previously bonded, send bond request */
                {
                    struct gapc_pairing pairing =
                    {
                        .iocap = GAP_IO_CAP_NO_INPUT_NO_OUTPUT,
                        .oob = GAP_OOB_AUTH_DATA_NOT_PRESENT,
                        .key_size = KEY_LEN,
                        .ikey_dist = (GAP_KDIST_IDKEY | GAP_KDIST_SIGNKEY),
                        .rkey_dist = (GAP_KDIST_ENCKEY | GAP_KDIST_IDKEY | GAP_KDIST_SIGNKEY),
#ifdef SECURE_CONNECTION
                        .auth = GAP_AUTH_REQ_SEC_CON_BOND,
                        .sec_req = GAP_SEC1_SEC_CON_PAIR_ENC
#else    /* ifdef SECURE_CONNECTION */
                        .auth = GAP_AUTH_REQ_NO_MITM_BOND,
                        .sec_req = GAP_NO_SEC
#endif
                    };
                    GAPC_BondCmd(conidx, &pairing);
                    PRINTF("    \n\rSending GAPC_BondCmd\n\r");
                }
#endif /* #if CFG_BOND_LIST_IN_NVR2 */
            }

            /* If not yet connected to all peers, keep scanning */
            if(GAPC_GetConnectionCount() < APP_NB_PEERS)
            {
                GAPM_StartConnectionCmd(&startConnectionCmd);
            }
        }
        break;

        case GAPC_DISCONNECT_IND:
        {
            GAPM_StartConnectionCmd(&startConnectionCmd); /* Re-start connection */
            PRINTF("\n\rGAPC_DISCONNECT_IND: reason = %d", ((struct gapc_disconnect_ind*)param)->reason);
        }
        break;

        case GAPC_PARAM_UPDATE_REQ_IND:
        {
            struct gapc_param_update_req_ind const *p = param;
            GAPC_ParamUpdateCfm(conidx, true, p->intv_min * 2, p->intv_max * 2);
            PRINTF("\n\rGAPC_PARAM_UPDATE_REQ_IND");
        }
        break;

        case GAPC_ENCRYPT_IND:
        {
            PRINTF("\n\rGAPC_ENCRYPT_IND: Link encryption is ON   auth: %d",((struct gapc_encrypt_ind *)param)->auth);
        }
        break;

#if CFG_BOND_LIST_IN_NVR2
        case GAPC_BOND_REQ_IND:
        {
            const struct gapc_bond_req_ind* p = param;
            bool accept = GAPC_IsBonded(conidx);
            switch (p->request)
            {
                case GAPC_IRK_EXCH:
                {
                    PRINTF("\n\rGAPC_BOND_REQ_IND / GAPC_IRK_EXCH");
                    union gapc_bond_cfm_data irkExch;
                    memcpy(irkExch.irk.addr.addr.addr, GAPM_GetDeviceConfig()->addr.addr, GAP_BD_ADDR_LEN);
                    irkExch.irk.addr.addr_type = GAPM_GetDeviceConfig()->addr_type;
                    memcpy(irkExch.irk.irk.key, GAPM_GetDeviceConfig()->irk.key, GAP_KEY_LEN);
                    GAPC_BondCfm(conidx, GAPC_IRK_EXCH, accept, &irkExch); /* Send confirmation */
                }
                break;

                case GAPC_CSRK_EXCH:
                {
                    PRINTF("\n\rGAPC_BOND_REQ_IND / GAPC_CSRK_EXCH");
                    union gapc_bond_cfm_data csrkExch;
                    memcpy(csrkExch.csrk.key, (uint8_t*)DEVICE_INFO_BLUETOOTH_CSRK, GAP_KEY_LEN);
                    GAPC_BondCfm(conidx, GAPC_CSRK_EXCH, accept, &csrkExch); /* Send confirmation */
                }
                break;
            }
        }
        break;
#endif /* CFG_BOND_LIST_IN_NVR2 */
    }
}

/* ----------------------------------------------------------------------------
 * Function      : void APP_LED_Timeout_Handler(ke_msg_idd_t const msg_id,
 *                                              void const *param,
 *                                              ke_task_id_t const dest_id,
 *                                              ke_task_id_t const src_id)
 * ----------------------------------------------------------------------------
 * Description   : Control GPIO "LED_DIO_NUM" behavior using a timer.
 *                 Possible LED behaviors:
 *                     - If the device is advertising but it has not connected
 *                       to any peer: the LED blinks every 200 ms.
 *                     - If the device is advertising and it is connecting to
 *                       fewer than BLE_CONNECTION_MAX peers: the LED blinks
 *                       every 2 seconds according to the number of connected
 *                       peers (i.e., blinks once if one peer is connected,
 *                       twice if two peers are connected, etc.).
 *                     - If the device is connected to BLE_CONNECTION_MAX peers
 *                       the LED is steady on.
 * Inputs        : - msg_id     - Kernel message ID number
 *                 - param      - Message parameter (unused)
 *                 - dest_id    - Destination task ID number
 *                 - src_id     - Source task ID number
 * Outputs       : None
 * Assumptions   : None
 * ------------------------------------------------------------------------- */
void APP_LED_Timeout_Handler(ke_msg_id_t const msg_id, void const *param,
                             ke_task_id_t const dest_id, ke_task_id_t const src_id)
{
    static uint8_t toggle_cnt = 0;
    uint8_t connectionCount = GAPC_GetConnectionCount();

    /* Blink LED according to the number of connections */
    switch (connectionCount)
    {
        case 0:
        {
            ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));
            Sys_GPIO_Toggle(LED_DIO_NUM); /* Toggle LED_DIO_NUM every 200ms */
            toggle_cnt = 0;
        }
        break;

        case APP_NB_PEERS:
        {
            ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));
            Sys_GPIO_Set_High(LED_DIO_NUM); /* LED_DIO_NUM steady high */
            toggle_cnt = 0;
        }
        break;

        default: /* connectionCount is between 1 and APP_NB_PEERS (exclusive) */
        {
            if (toggle_cnt >= connectionCount * 2)
            {
                toggle_cnt = 0;
                ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_S(2)); /* Schedule timer for a long 2s break */
                Sys_GPIO_Set_High(LED_DIO_NUM); /* LED_DIO_NUM steady high until next 2s blinking period */
            }
            else
            {
                toggle_cnt++;
                Sys_GPIO_Toggle(LED_DIO_NUM);
                ke_timer_set(APP_LED_TIMEOUT, TASK_APP, TIMER_SETTING_MS(200));
            }
        }
    }
}

