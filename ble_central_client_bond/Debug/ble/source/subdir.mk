################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ble/source/ble_gap.c \
../ble/source/ble_gatt.c \
../ble/source/ble_l2c.c \
../ble/source/msg_handler.c 

OBJS += \
./ble/source/ble_gap.o \
./ble/source/ble_gatt.o \
./ble/source/ble_l2c.o \
./ble/source/msg_handler.o 

C_DEPS += \
./ble/source/ble_gap.d \
./ble/source/ble_gatt.d \
./ble/source/ble_l2c.d \
./ble/source/msg_handler.d 


# Each subdirectory must supply rules for building sources it contributes
ble/source/%.o: ../ble/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g -DRSL10_CID=101 -DSECURE_CONNECTION -DAPP_BONDLIST_SIZE=28 -DCFG_BOND_LIST_IN_NVR2=true -DCFG_BLE=1 -DCFG_ALLROLES=1 -DCFG_CON=4 -DCFG_APP -DCFG_APP_BATT -DCFG_ATTC=1 -DCFG_EMB=1 -DCFG_HOST=1 -DCFG_RF_ATLAS=1 -DCFG_ALLPRF=1 -DCFG_PRF=1 -DCFG_NB_PRF=2 -DCFG_CHNL_ASSESS=1 -DCFG_SEC_CON=1 -DCFG_EXT_DB -DCFG_PRF_BASC=1 -I"Z:\Hardware\homes\Yehuda\FW of RSL10\BitBucket\Client\ble_central_client_bond\ble\include" -I"Z:\Hardware\homes\Yehuda\FW of RSL10\BitBucket\Client\ble_central_client_bond\ble_profiles\include" -I"Z:\Hardware\homes\Yehuda\FW of RSL10\BitBucket\Client\ble_central_client_bond\include" -I"C:\Program Files (x86)\ON Semiconductor\RSL10 SDK\eclipse\..\include\bb" -I"C:\Program Files (x86)\ON Semiconductor\RSL10 SDK\eclipse\..\include\ble\profiles" -I"C:\Program Files (x86)\ON Semiconductor\RSL10 SDK\eclipse\..\include\ble" -I"C:\Program Files (x86)\ON Semiconductor\RSL10 SDK\eclipse\..\include\kernel" -I"C:\Program Files (x86)\ON Semiconductor\RSL10 SDK\eclipse\..\include" -std=gnu11 -Wmissing-prototypes -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


