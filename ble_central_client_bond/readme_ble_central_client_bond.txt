               Pairing and Bonding with Central Device Sample Code
               ===================================================

NOTE: If you use this sample application for your own purposes, follow
      the licensing agreement specified in Software_Use_Agreement.rtf
      in the home directory of the installed RSL10 Software
      Development Kit (SDK).

Overview
--------
This sample project generates a battery service and a custom service client. 
It then connects up to four peripheral peer devices in direct connectable mode 
with known Bluetooth peer addresses. Once the connection is established with 
any peer device, the central device attempts to pair/bond and start 
encryption. It also starts battery/custom service discovery for that device. 
If the services are discovered, the application periodically sends read 
requests for the battery level and custom service attributes, using a kernel 
timer. The application stores the bond information in the RSL10 NVR2 flash, 
and can perform Private Address Resolution. Upon reconnection, the application 
starts encryption using the saved bond information.

This sample project is an enhanced version of central_client_bond. It is 
equipped with a Bluetooth low energy abstraction layer that provides a higher 
level application programming interface (API) to abstract the Bluetooth GAP 
and GATT layers. The abstraction layer has been designed to improve 
flexibility and simplicity by providing the following features:
    - An event subscription mechanism that allows the application to subscribe 
      and receive callback notifications for any Kernel or Bluetooth event. 
      This improves encapsulation/integration of different modules, as they 
      can be implemented while isolated in their own files.
    - Generic definition of custom services with callback notification support
    - Security handling and bond list implementation in RSL10 flash
    - Code structure and API naming aligned with RivieraWaves documentation, 
      so you can map the code into the documentation more easily

The sample code was refactored to keep the generic abstraction layer code 
and application-specific code in separate files. This increases flexibility, 
maintainability and reusability of components between distinct applications.

Once the connection is established, the central device starts battery 
service and custom service discovery. The application sends a read request for 
the battery level value every five seconds, using a kernel timer. If the 
custom service and related characteristics are discovered, the application 
sends a write request for one of the custom attributes every two seconds. 
For the custom service, if the custom service UUID and two characteristics 
UUID are discovered, the variable cs_env.state is set to 
CS_ALL_ATTS_DISCOVERED. In parallel, when connection is established, the 
application sends a pairing request, handles all the key exchanges, and stores
the values when they are exchanged.

The code can be modified to achieve the desired security level and to specify 
which keys will be exchanged. The remote keys (IRK/CSRK/LTK) are received in 
the GAPC_MsgHandler function (implemented inside the Bluetooth low energy 
abstraction file ble_gap.c) and automatically saved in a temporary variable 
(BondInfo_Type) during the pairing process. The local IRK and CSRK are read 
from NVR3 as default device security information. The received LTK, remote IRK
and CSRK values are saved into non-volatile record two (NVR2) in flash after 
pairing succeeds.

Erasing NVR2 will lead to an empty bondlist. If the desired behavior of this
application or test configuration is to start from a new bondlist at startup, 
the following steps can be undertaken:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which will
    pause at the start of its initialization routine).

Note that this will erase the flash if and only if there is anything in the NVR2 drive.

The peers' addresses are set through macros (DIRECT_PEER_BD_ADDRESS|1|2|3|4) 
in app.h.

The device address type and source are set in app.h. If APP_DEVICE_PARAM_SRC
is set to FLASH_PROVIDED_or_DFLT and the address type is set to 
GAPM_CFG_ADDR_PUBLIC, the stack loads the public device address stored in NVR3
flash. Otherwise, the address provided in the application is used.

The message subscription mechanism allows the application and services to 
subscribe and receive callback notifications based on the Kernel message ID 
or task ID. This allows each module of the application to be independently 
implemented in its own files. The subscription is performed using the 
MsgHandler_Add() function. The application subscribes to receive GAPM and 
GAPC events (see app.c). The services subscribe to receive Kernel events in 
their initialization function (see BASC_Initialize() in ble_basc.c, for an 
example). The application event handlers are implemented in app_msg_handler.c.

The basic sequence of operations and event messages exchanged between the 
application and the Bluetooth stack is presented below: 

  APP <--> Bluetooth low energy Stack
  Startup
      --->  GAPM_ResetCmd() - app.c
      <---  GAPM_CMP_EVT / GAPM_RESET
      --->  GAPM_SetDevConfigCmd() - app_msg_handler.c
      <---  GAPM_CMP_EVT / GAPM_SET_DEV_CONFIG
      --->  GAPM_ProfileTaskAddCmd() - ble_basc.c
      <---  GAPM_PROFILE_ADDED_IND
      --->  GAPM_StartConnectionCmd() - app_msg_handler.c
            
  Connection request / parameters update request / bond/encrypt request
      <---  GAPC_CONNECTION_REQ_IND
      --->  GAPM_ResolvAddrCmd() - app_msg_handler.c
      --->  GAPM_ADDR_SOLVED_IND
      --->  GAPC_ConnectionCfm()
      <---  GAPC_PARAM_UPDATE_REQ_IND
      --->  GAPC_ParamUpdateCfm() - app_msg_handler.c
      If bonded previously: 
      --->  GAPC_EncryptCmd() - app_msg_handler.c
      <---  GAPC_ENCRYPT_IND
      If not previously bonded - app_msg_handler.c
      --->  GAPC_BondCmd() - app_msg_handler.c
      <---  GAPC_IRK_EXCH
      --->  GAPC_BondCfm() - app_msg_handler.c
      <---  GAPC_CSRK_EXCH
      --->  GAPC_BondCfm() - app_msg_handler.c
      If connection count < APP_NB_PEERS
      --->  GAPM_StartConnectionCmd() - app_msg_handler.c

  Disconnection
      <---  GAPC_DISCONNECT_IND
      --->  GAPM_StartConnectionCmd() - app_msg_handler.c

This sample project is structured as follows:

The source code exists in a "source" folder. Application-related header files 
are in the "include" folder. The main() function is implemented in the "app.c"
file, which is located in the parent directory.
The Bluetooth low energy abstraction layer is placed in two separate folders: 
"ble" and "ble_profiles". The "ble" folder contains support functions for the 
GAP and GATT layers. It also has a message handling mechanism that allows the 
application to subscribe to Kernel events. The "ble_profiles" folder contains
standard profiles-related files.


Application-specific files
--------------------------
app.c                  - Main application file 

source
------
    app_config.c      - Device configuration and definition of application-
                        specific Bluetooth Low Energy messages.
    app_msg_handler.c - Application-specific message handlers
    app_basc.c        - Application-specific battery service client message 
                        handler
    app_customsc.c    - Application-specific custom service client
    app_trace.c       - Debugging (printf) utility functions source

include
-------
    app.h             - Main application header file
    app_basc.h        - Application-specific battery service client header
    app_customss.h    - Application-specific custom service client header
    app_trace.h       - Debugging (printf) utility functions header
    
Bluetooth low energy Abstraction files (generic for any application)
---------------------
ble\source
----------
    ble_gap.c         - GAP layer support functions and message handlers          
    ble_gatt.c        - GATT layer support functions and message handlers               
    msg_handler.c     - Message handling subscribing mechanism implementation

ble\include
-----------
    ble_gap.h         - GAP layer abstraction header
    ble_gatt.h        - GATT layer abstraction header
    msg_handler.h     - Message handling subscribing mechanism header       
    
ble_profiles\source
-------------------
    ble_basc.c        - Battery Service Server support functions and handlers
                     
ble_profiles\include
--------------------
    ble_basc.h        - Battery Service Server header


Hardware Requirements
---------------------
This application can be executed on any RSL10 Evaluation and Development Board
with no external connections required.

Importing a Project
-------------------
To import the sample code into your IDE workspace, refer to the 
Getting Started Guide for your IDE for more information.

Select the project configuration according to the required optimization level. 
Use "Debug" configuration for optimization level "None" or "Release"
configuration for optimization level "More" or "O2".

Verification
------------
To verify if this application is functioning correctly, use another RSL10 
board with the ble_peripheral_server_bond application or another third-party 
peripheral device application to advertise while this application establishes 
a connection and ensures pairing/bonding/encryption is successfully performed. 
In addition to establishing a connection, this application can be used to 
read/write characteristics and receive notifications.

Alternatively, you can observe the behavior of the LED on the RSL10 Evaluation 
and Development Board (DIO6). The LED behavior is controlled by the 
APP_LED_Timeout_Handler function (in app_msg_handler.c) and can be one of the 
following:

    - If the device has not started a connection procedure, the LED is off.
    - If the device started a connection procedure but it is not connected to 
      any peer, the LED blinks every 200ms.
    - If the device is connected to fewer than APP_NB_PEERS peers, the LED
      blinks every 2 seconds according to the number of connected peers 
      (i.e., blinks once if one peer is connected, twice if two peers are 
       connected, etc.).
    - If the device is connected to APP_NB_PEERS peers, the LED is steady on.

Notes
-----
Sometimes the firmware in RSL10 cannot be successfully re-flashed, due to the
application going into Sleep Mode or resetting continuously (either by design 
or due to programming error). To circumvent this scenario, a software recovery
mode using DIO12 can be implemented with the following steps:

1.  Connect DIO12 to ground.
2.  Press the RESET button (this restarts the application, which will
    pause at the start of its initialization routine).
3.  Re-flash RSL10. After successful re-flashing, disconnect DIO12 from
    ground, and press the RESET button so that the application will work
    properly.

It should be noted that the use of this method will also reset the NVR2 memory.
==============================================================================
Copyright (c) 2018 Semiconductor Components Industries, LLC
(d/b/a ON Semiconductor).
