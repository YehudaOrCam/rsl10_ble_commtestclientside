/* ----------------------------------------------------------------------------
 * Copyright (c) 2018 Semiconductor Components Industries, LLC (d/b/a
 * ON Semiconductor), All Rights Reserved
 *
 * Copyright (C) RivieraWaves 2009-2018
 *
 * This module is derived in part from example code provided by RivieraWaves
 * and as such the underlying code is the property of RivieraWaves [a member
 * of the CEVA, Inc. group of companies], together with additional code which
 * is the property of ON Semiconductor. The code (in whole or any part) may not
 * be redistributed in any form without prior written permission from
 * ON Semiconductor.
 *
 * The terms of use and warranty for this code are covered by contractual
 * agreements between ON Semiconductor and the licensee.
 *
 * This is Reusable Code.
 *
 * ----------------------------------------------------------------------------
 * app.c
 * - Main application file
 * ----------------------------------------------------------------------------
 * $Revision: 1.3 $
 * $Date: 2018/06/28 04:25:02 $
 * ------------------------------------------------------------------------- */

#include <app.h>

int main(void)
{
    /* Configure hardware and initialize BLE stack */
    Device_Initialize();
    UART_Initialize();

    /* Debug/trace initialization. In order to enable UART or RTT trace,
     * configure the 'RSL10_DEBUG' macro in app_trace.h */
    TRACE_INIT();
    sprintf(UART_WriteBuffer, "ble_central_client has started 1.0.0! \r\n");
	UART_ScreenRight(UART_WriteBuffer);


    /* Initialize Battery Service Client */
    BASC_Initialize();
    BASC_RequestBattLevelOnTimeout(TIMER_SETTING_S(5));

    /* Initialize Custom Service Client */
    CUSTOMSC_Initialize();

    /* Add application message handlers */
    MsgHandler_Add(TASK_ID_GAPM,    APP_GAPM_Handler);
    MsgHandler_Add(TASK_ID_GAPC,    APP_GAPC_Handler);
    MsgHandler_Add(APP_LED_TIMEOUT, APP_LED_Timeout_Handler);
    MsgHandler_Add(BASC_BATT_LEVEL_IND, APP_BASC_BattLevelInd_Handler);

    /* Reset the GAP manager. Trigger GAPM_CMP_EVT / GAPM_RESET when finished.
     * See APP_GAPM_Handler */
    GAPM_ResetCmd();

    while (1)
    {
        Kernel_Schedule();

        /* Refresh the watchdog timer */
        Sys_Watchdog_Refresh();

        /* Wait for an event before executing the scheduler again */
        SYS_WAIT_FOR_EVENT;
    }
}
